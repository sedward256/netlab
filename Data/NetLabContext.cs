﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NetLab.Models;

namespace NetLab.Data
{
    public class NetLabContext : DbContext
    {
        public NetLabContext (DbContextOptions<NetLabContext> options)
            : base(options)
        {
        }

        public DbSet<Movie> Movie { get; set; }

        public DbSet<Genre> Genres { get; set; }
    }
}
