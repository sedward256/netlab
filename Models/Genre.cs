using System.ComponentModel;

namespace NetLab.Models
{

    public class Genre 
    {
        public int Id { get; set; }

        [DisplayName("Name")]
        public string Name { get; set; }

        [DisplayName("Description")]
        public string Description { get; set; }
    }

}